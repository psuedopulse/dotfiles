#! /bin/sh

mv $HOME/.config/polybar $HOME/.config/polybar-old
mv $HOME/.config/alacritty $HOME/.config/alacritty-old
mv $HOME/.config/rofi $HOME/.config/rofi-old
mv $HOME/.config/i3 $HOME/.config/i3-old
mv $HOME/.config/dunst $HOME/.config/dunst-old

cp -r ./polybar $HOME/.config/polybar
cp -r ./alacritty $HOME/.config/alacritty
cp -r ./rofi $HOME/.config/rofi
cp -r ./i3 $HOME/.config/i3
cp -r ./dunst $HOME/.config/dunst

mv $HOME/.zshrc $HOME/.zshrc.bak
cp -r ./.zshrc $HOME/.zshrc
