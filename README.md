# Dotfiles

# Requirements
for binaries, at least mv and cp are needed for the install script to work

this repo includes dotfiles for the following programs
 - i3-gaps
 - polybar (with i3 support enabled)
 - dunst
 - rofi
 - alacritty
 - zsh

# Installation

```
git clone https://gitlab.com/psuedopulse/dotfiles
cd dotfiles
chmod +x install.sh
./install.sh
```

if you have any configurations for any of the included programs, the folders containing them will be renamed to (program)-old to preserve them (for zsh it's .zshrc.bak)
