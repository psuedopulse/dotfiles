# ZSHRC

# plugins
# source ~/Repos/zsh-autocomplete/zsh-autocomplete.plugin.zsh

source /home/psuedopulse/.cache/antibody/https-COLON--SLASH--SLASH-github.com-SLASH-popstas-SLASH-zsh-command-time/command-time.plugin.zsh

fpath+=( /home/psuedopulse/.cache/antibody/https-COLON--SLASH--SLASH-github.com-SLASH-popstas-SLASH-zsh-command-time )

source /home/psuedopulse/.cache/antibody/https-COLON--SLASH--SLASH-github.com-SLASH-zsh-users-SLASH-zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
fpath+=( /home/psuedopulse/.cache/antibody/https-COLON--SLASH--SLASH-github.com-SLASH-zsh-users-SLASH-zsh-autosuggestions )

# autoload -Uz add-zsh-hook vcs_info

autoload compinit && compinit

# zstyle ':completion:*' menu select

SAVEHIST=1000  # Save most-recent 1000 lines
HISTFILE=~/.zsh_history

# setopt COMPLETE_ALIASES
setopt prompt_subst

# add-zsh-hook precmd vcs_info

ZSH_COMMAND_TIME_MSG=''

ZSH_AUTOSUGGEST_STRATEGY=(history completion)

# ALIASES

# Git

alias ga='git add'
alias gm='git commit'
alias gpsh='git push'
alias gpl='git pull'
alias gc='git clone'

# QoL

alias clear='clear && neofetch'
alias rmdir='rm -rf'
alias cp='cp -r'
alias dot='cd ~/.config && ls --color=auto'
alias pbar='$HOME/.config/polybar/launch.sh'
alias update='$HOME/Scripts/update.sh'
alias dotsync='$HOME/Scripts/dotsync.sh'
alias kmpd='sudo kill -9 $(pidof mpd)'
alias src='source ~/.zshrc'

# PROMPT

PS1='%F{blue}%n%F %F{green}@%F %F{blue}%m%F %F{magenta}[%F%F{cyan}%D{%I:%M}%F%F{magenta}]%F %F{purple}﬋%F '$'\n'' %F{magenta}%~%F %(!.%F{red}>> %F.%F{green}>> %F) %F{cyan}'
RPS1='%F{green}[%F%F{cyan}%?%F%F{yellow}:%F%F{purple}%h%F%F{green}]%F'

# settings
# zstyle ':vcs_info:' check-for-changes true
# zstyle ':vcs_info:' unstagedstr ' *'
# zstyle ':vcs_info:' stagedstr ' +'
# zstyle ':vcs_info:git:*' formats '%b%u%c'
# zstyle ':vcs_info:git:*' actionformats '%b|%a%u%c'

# AUTOSTART
clear
