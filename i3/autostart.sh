#! /bin/bash

sudo killall -q mpd
sudo killall -q picom
sudo killall -q polkit-gnome-authentication-agent-1

sudo /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & disown

bash /home/psuedopulse/.config/polybar/launch.sh & disown

mpd & disown
picom -f --vsync & disown


